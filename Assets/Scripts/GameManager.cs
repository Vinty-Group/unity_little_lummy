﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GameState {
    MainMenu,
    Loading,
    Game,
    GamePause,
    Death
}

public class GameManager : MonoBehaviour {
    [SerializeField] private GameObject   textLife;
    [SerializeField] private GameObject   textLevel;
    [SerializeField] private GameObject[] respawn;

    private GameObject _player;
    private Vector2    _currentRespawn;
    private int _life = 5;
    private int _indexRespawn;

    public int IndexRespawn {
        set => _indexRespawn = value;
    }

    public static  GameState CurrentGameState {get; private set;}
    public static Action<GameState> GameStateAction;
    public IPlayer      player;
    public List<IEnemy> enemies = new List<IEnemy>();

    private void Start() {
        _player = GameObject.Find("Player");
        SaveIndexRespawn();
        _indexRespawn = Convert.ToInt32(PlayerPrefs.GetString("indexRespawn"));
        textLife.GetComponent<TextMeshPro>().SetText("x " + _life);
        _currentRespawn = respawn[_indexRespawn].transform.position;

        SetGameState(GameState.Game);
    }

    private void Update() {
        GameOver();
    }

    public static void SetGameState(GameState newState) {
        CurrentGameState = newState;
        if(GameStateAction != null) {
            GameStateAction.Invoke(newState);
        }
    }

    private IEnumerator WaitAndPrintStart() {
        yield return new WaitForSeconds(1f);
    }

    public void MinusLife() {
        if(CurrentGameState != GameState.Game) {
            return;
        }

        SetGameState(GameState.Death);
        _life--;
        textLife.GetComponent<TextMeshPro>().SetText("x " + _life);
        StartCoroutine(DeathAnimation());
    }

    private IEnumerator DeathAnimation() {
        yield return new WaitForSeconds(3f);
        _player.transform.position = _currentRespawn;
        _currentRespawn = respawn[_indexRespawn].transform.position;
        SetGameState(GameState.Game);
    }

    public void GameOver() {
        if(_life <= 0) {
            _indexRespawn = 0;
            SetGameState(GameState.Game);
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public void SaveIndexRespawn() {
        PlayerPrefs.SetString("indexRespawn", _indexRespawn.ToString());
    }
}
