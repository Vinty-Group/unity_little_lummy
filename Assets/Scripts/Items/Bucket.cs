﻿using UnityEngine;

namespace Items{
    public class Bucket : Item{
        private const string NAME        = "Ведро";
        private const string DESCRIPTION = "В этом ведре можно носить воду!";

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public Bucket(string name, string description) : base(name, description) {
        }
    }
}