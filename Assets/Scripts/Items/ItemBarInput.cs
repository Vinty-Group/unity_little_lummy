﻿using System;
using UnityEngine;

namespace Items{
    public class ItemBarInput : MonoBehaviour{
        private ItemBar _itemBar;

        private void Start() {
            _itemBar = FindObjectOfType<ItemBar>();
        }

        private void OnGUI() {
            Event     e       = Event.current;
            EventType evnType = EventType.KeyDown;
            if(e.isKey && e.type == evnType) {
                if((e.keyCode == KeyCode.Alpha1
                 || e.keyCode == KeyCode.Alpha2
                 || e.keyCode == KeyCode.Alpha3
                 || e.keyCode == KeyCode.Alpha4
                 || e.keyCode == KeyCode.Alpha5
                    )) { PressDownAlpha(e.keyCode); }
            }
        }

        public void PressDownAlpha(KeyCode keyCode) {
            string substringNumber    = keyCode.ToString().Substring(5, 1);
            int    numberFromInputKey = Convert.ToInt32(substringNumber);
            if(numberFromInputKey < 6) { _itemBar.getItemFromItemsBar(numberFromInputKey - 1); }
        }
    }
}
