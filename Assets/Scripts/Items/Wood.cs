﻿using UnityEngine;

namespace Items{
    public class Wood : Item{
        private const string NAME        = "Дерево";
        private const string DESCRIPTION = "Из дерева можно сделать много разных вещей...";

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public Wood(string name, string description) : base(name, description) {
        }
    }
}