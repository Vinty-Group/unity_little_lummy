﻿using Player;
using UnityEngine;

namespace Items {
    public class TrapCell : Item {
        private const string NAME        = "Клетка с приманкой";
        private const string DESCRIPTION = "Запах сыра внутри ловушки наверняка привлечет крысу.";
        private const string RAT_ENEMY   = "RatEnemy";
        private const string CELL_WITH_RAT = "CellWithRat"; 

        private PlayerController _playerController;
        private GameObject _ratEnemy;

        private void Start() {
            _playerController = FindObjectOfType<PlayerController>();
            _ratEnemy = GameObject.Find(CELL_WITH_RAT);
        }

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public TrapCell(string name, string description) : base(name, description) {
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if(other.gameObject.name.Equals(RAT_ENEMY) && !_playerController.ItemInHand.name.Equals(RAT_ENEMY)
               ) {
                _ratEnemy.transform.position = gameObject.transform.position;
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
