﻿using UnityEngine;

namespace Items{
    public class Hammer : Item{
        private const string NAME        = "Молот";
        private const string DESCRIPTION = "Таким молотком можно разбивать камни! Кто знает, что внутри...";

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public Hammer(string name, string description) : base(name, description) {
        }
    }
}