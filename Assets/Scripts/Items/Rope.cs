﻿using UnityEngine;

namespace Items{
    public class Rope : Item{
        private const string NAME        = "Топор";
        private const string DESCRIPTION = "Очень полезный предмет! можно связать вместе что угодно.";

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public Rope(string name, string description) : base(name, description) {
        }
    }
}