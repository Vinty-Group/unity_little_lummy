﻿using UnityEngine;

namespace Items{
    public class Cheese : Item{
        private const string NAME        = "Сыр";
        private const string DESCRIPTION = "Такой сыр нравится грызунам. Пищат с него!";

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public Cheese(string name, string description) : base(name, description) {
        }
    }
}