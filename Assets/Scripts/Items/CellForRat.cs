﻿using Player;
using UnityEngine;

namespace Items {
    public class CellForRat : Item {
        private const string NAME            = "Клетка";
        private const string DESCRIPTION     = "В такую клетку можно словить грызуна.";
        private const string CHEESE_FOR_CELL = "Cheese";

        private GameObject trapCell;

        private PlayerController _playerController;

        private void Start() {
            _playerController = FindObjectOfType<PlayerController>();
            trapCell = GameObject.Find("TrapCell");
        }

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public CellForRat(string name, string description) : base(name, description) {
        }

        private void OnTriggerEnter2D(Collider2D other) {
            if(other.gameObject.name.Equals(CHEESE_FOR_CELL) &&
               _playerController.ItemInHand.name.Equals(CHEESE_FOR_CELL)) {
                trapCell.transform.position = gameObject.transform.position ;
                Destroy(other.gameObject);
                Destroy(gameObject);
            }
        }
    }
}
