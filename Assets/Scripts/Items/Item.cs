﻿using UnityEngine;

namespace Items{
    public abstract class Item : MonoBehaviour{
        private readonly string _name;
        private readonly string _description;

        protected Item(string name, string description) {
            _name        = name;
            _description = description;
        }

        public new virtual string getName() {
            return _name;
        }

        public new virtual string getDescription() {
            return _description;
        }
    }
}