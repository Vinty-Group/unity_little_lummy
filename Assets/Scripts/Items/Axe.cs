﻿using UnityEngine;

namespace Items{
    public class Axe : Item{
        private const string NAME        = "Топор";
        private const string DESCRIPTION = "Такой топор легко может нарубить веток с дерева...";

        public override string getName() {
            return NAME;
        }

        public override string getDescription() {
            return DESCRIPTION;
        }

        public Axe(string name, string description) : base(name, description) {
        }
    }
}