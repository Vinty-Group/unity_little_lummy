﻿using System;
using Player;
using UnityEngine;

namespace Items{
    public class ItemBar : MonoBehaviour{
        private       PlayerController _playerController;
        public        GameObject[]     itemsBar;
        private const float            DELTA_Z_FOR_VISIBLE_ON_SCENE = 1;

        private void Start() {
            _playerController = FindObjectOfType<PlayerController>();
        }

        public void setItemToFreeCell(GameObject obj) {
            foreach(GameObject t in itemsBar) {
                GameObject currentObj = t;
                if(currentObj.transform.childCount != 0) { continue; }

                Vector3 transformPosParent = currentObj.transform.position;
                Vector3 targetPosition = new Vector3(
                    transformPosParent.x,
                    transformPosParent.y,
                    transformPosParent.z - DELTA_Z_FOR_VISIBLE_ON_SCENE
                );
                obj.transform.position = targetPosition;
                obj.transform.SetParent(currentObj.transform);
                //              itemsBar[i] = obj;
                return;
            }
        }

        public void getItemFromItemsBar(int index) {
            int countChildInt = itemsBar[index].transform.childCount;
            if(countChildInt != 1) { return; }

            GameObject obj = itemsBar[index].transform.GetChild(0).gameObject;
            // if(!playerController.IsLeftHandIsEmpty()) { return; }

            _playerController.TakeItInTheLeftHand(obj);
            _playerController.SetLeftHandBusy();
        }
    }
}
