﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour{
    private static          string _nextLevel;
    private static readonly int    fade = Animator.StringToHash("Fade");

    [SerializeField] private GameObject loadingScreen;
    [SerializeField] private Image      loadingImg;
    [SerializeField] private Text       progressText;
    [SerializeField] private Animator   _animator;

    private bool _isProgressBarReady = true;

    private IEnumerator Start() {
        GameManager.SetGameState(GameState.Loading);
        yield return new WaitForSeconds(0.5f);
        if(string.IsNullOrEmpty(_nextLevel)) {
            SceneManager.LoadScene("MainMenu");
            yield break;
        }

        AsyncOperation operation = null;
        operation = SceneManager.LoadSceneAsync(_nextLevel, LoadSceneMode.Additive);
        loadingScreen.SetActive(true);
        float progress = 0.1f;
        while(_isProgressBarReady) {
            // while(!operation.isDone) {
            // честный рассчет прогрес-бара
            // float progress = Mathf.Clamp01(operation.progress / 0.9f);

            progress              += 0.1f;
            loadingImg.fillAmount =  progress;
            yield return new WaitForSeconds(0.2f);
            progressText.text = string.Format("{0:0}%", progress * 100);
            yield return null;
            if(progress >= 0.9) {
                _animator.SetTrigger(fade);
                _isProgressBarReady = false;
                yield break;
            }
        }

        yield return new WaitForSeconds(3f);
        _nextLevel = null;
        SceneManager.UnloadSceneAsync("LoadingScene");
    }

    public static void LoadLevel(string level) {
        _nextLevel = level;
        SceneManager.LoadScene("LoadingScene");
    }
}
