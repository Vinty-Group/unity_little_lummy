﻿using System.Collections;
using System.Collections.Generic;
using Enemy;
using UnityEngine;

public class PatrolEnemy : BaseEnemy
{
    [SerializeField] private float speed ;
    [SerializeField] private float distance;
    [SerializeField] private float scale; 
    // Start is called before the first frame update
    public void Update()
    {
        MovePatrol();
    }
    
    private void MovePatrol()
    {
        {
            transform.Translate(speed * Time.deltaTime,0,0);
            if (transform.position.x > distance)
            {
                speed = -speed;
                transform.localScale = new Vector3(-scale,scale,scale);
                //  enemyRotate = true;
            }
 
            if (transform.position.x < -distance)
            {
                speed = - speed;
                transform.localScale = new Vector3(scale,scale,scale);
            }
        }  
    }
    
}
