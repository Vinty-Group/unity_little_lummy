﻿using System;
using UnityEngine;

namespace Enemy {
    public class TowerTrigger : MonoBehaviour {
        [SerializeField] private GameObject _explosion;

        private Bomb _bomb;

        private void Start() {
            _bomb = FindObjectOfType<Bomb>();
        }

        private void OnTriggerEnter2D(Collider2D other) {
            string nameObj = other.gameObject.name;
            if(nameObj.Equals("Bomb") && _bomb.IsBombActivated) {
                if(other.gameObject != null) {
                    GameObject explosionAnim = Instantiate(_explosion);
                    explosionAnim.transform.position = gameObject.transform.position;
                    Destroy(other.gameObject);
                    Destroy(gameObject);
                }
            }
        }
    }
}
