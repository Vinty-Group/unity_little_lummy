﻿using System.Collections;
using UnityEngine;

public class HitBullet : MonoBehaviour {
    [SerializeField] private Transform  shootPoint;
    [SerializeField] private Transform  player;
    [SerializeField] private GameObject bullet;

    private const float MAX_SPEED = 2f;
    private       bool  _isPlayerNear;
    private       bool  _isTowerLive;
    private       float _waitForShoot = 3.3f;

    public bool isTowerLive {
        set => _isTowerLive = value;
    }

    public bool IsPlayerNear {
        set => _isPlayerNear = value;
    }

    private void FixedUpdate() {
        if(_isPlayerNear && _isTowerLive) {
            StartCoroutine(StartTowerAttack());
            _isPlayerNear = false;
            StopCoroutine(StartTowerAttack());
        }
    }

    public void Shoot() {
        Vector2     shootPointPos = shootPoint.position;
        Vector2     playerPos     = player.transform.position;
        Transform   instBullet    = CreateBullet();
        Rigidbody2D rigBullet     = instBullet.GetComponent<Rigidbody2D>();
        StartCoroutine(MoveBullet(shootPointPos, playerPos, rigBullet));
    }

    private Transform CreateBullet() {
        Transform instBullet = Instantiate(bullet.transform);
        instBullet.transform.position = shootPoint.transform.position;
        return instBullet;
    }

    private IEnumerator MoveBullet(Vector2 shootPointPos, Vector2 playerPos, Rigidbody2D bull) {
        while(true) {
            Vector2 vector = CalculateTrajectoryVelocity(shootPointPos, playerPos, MAX_SPEED);
            if(bull != null) {
                bull.velocity = vector;
            }

            yield return new WaitForSeconds(3f);
            if(bull != null) {
                Destroy(bull.gameObject);
            }
        }
    }

    private IEnumerator StartTowerAttack() {
        if(_isPlayerNear) {
            _isPlayerNear = false;
            Shoot();
        }

        yield return new WaitForSeconds(_waitForShoot);
        _isPlayerNear = true;
    }

    public void StopTowerAttack() {
        _isPlayerNear = false;
        _isTowerLive  = false;
        StopCoroutine(StartTowerAttack());
    }

    private Vector3 CalculateTrajectoryVelocity(Vector2 shootPointPosition, Vector2 playerPos, float speed) {
        float velocityX = (playerPos.x - shootPointPosition.x) / speed;
        float velocityY = (playerPos.y - shootPointPosition.y) / speed;
        return new Vector2(velocityX, velocityY);
    }
}
