﻿using UnityEngine;

namespace Enemy{
    public class BaseEnemy : MonoBehaviour{
        protected virtual void GetDamage() {
            Die();
        }

        protected virtual void Die() {
            Destroy(gameObject);
        }
    }
}