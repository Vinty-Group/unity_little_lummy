﻿using UnityEngine;

namespace Enemy{
    public class WalkEnemy : BaseEnemy{
        [SerializeField] private float speed;

        private bool _isFacingRight;

        private void OnTriggerEnter2D(Collider2D other) {
            if(other.CompareTag("Wall")) { Flip(); }
        }

        private void Update() {
            MoveMonsters();
        }

        private void MoveMonsters() {
            transform.Translate(speed * Time.deltaTime, 0, 0);
        }

        private void Flip() {
            {
                transform.Rotate(0, 180, 0);
            }
        }
    }
}