﻿using System;
using UnityEngine;
using UnityEngine.Serialization;

namespace Sound{
    public class Sounds : MonoBehaviour{
        [SerializeField] private AudioSource step;
        [SerializeField] private AudioSource jump;
        [SerializeField] private AudioSource takeItem;
        [SerializeField] private float       stepTimer = 0.7f; //скорость звука шага
        [SerializeField] private float       volume    = 0.1f; //громкость (максимальное значение = 1,0)

        private float _stepTimerDown;

        public void PlayStepSound() {
            if(_stepTimerDown > 0) {
                _stepTimerDown -= Time.deltaTime;
            }

            if(_stepTimerDown < 0) {
                _stepTimerDown = 0;
            }
                                                                                                                                                                                                                                                                                            
            if(Math.Abs(_stepTimerDown) < 0.0001f) {
                step.PlayOneShot(step.clip, volume);
                _stepTimerDown = stepTimer;
            }
        }

        public void PlaySoundTakeItem() {
            step.PlayOneShot(takeItem.clip, volume + 0.5f);
        }

        public void PlaySoundJump() {
            jump.PlayOneShot(jump.clip, volume);
        }
    }
}
