﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class LummyCamera : MonoBehaviour
{
    [SerializeField] private float distance = -9f;
    [SerializeField] private float height = 6f;
    private GameObject _player;

    private void Start() {
        _player = GameObject.Find("Player");
    }

    private void Update()
    {
        Vector3 position = _player.transform.position;
        Vector3 playerPosition = new Vector3(position.x, position.y + height, position.z + distance);
        transform.position = Vector3.Lerp(transform.position, playerPosition, Time.deltaTime * 2f);
    }
}
