﻿using UnityEngine;

public class Bomb : MonoBehaviour {
    [SerializeField] private GameObject     bombPoint;
    [SerializeField] private GameObject     _flickerBomb;
    private                  SpriteRenderer _flickerSprite;

    public bool IsBombActivated {get; private set;}

    public void BombActivated() {
        if(bombPoint.transform.childCount != 0) {
            return;
        }

        // Создаем фликер и присоединяем к бомбе
        GameObject flickerObj = Instantiate(_flickerBomb, gameObject.transform, true);
        _flickerSprite = flickerObj.GetComponent<SpriteRenderer>();
        // Перемещаем фликер к бомбе
        flickerObj.transform.position = bombPoint.transform.position;
        // включаем мерцание
        _flickerSprite.enabled = true;
        IsBombActivated        = true;
    }
}
