﻿using InfoPanels;
using Items;
using Sound;
using UnityEngine;

namespace Player{
    public class PlayerBasicInput : MonoBehaviour{
        private PlayerTrigger    _trigger;
        private PlayerController _controller;
        private PlayerAnimation  _playerAnimation;
        private DialogWindow     _dialogWindow;
        private Sounds           _sound;

        private static readonly int playerRunRight = Animator.StringToHash("PlayerRunRight");
        private static readonly int playerRunLeft  = Animator.StringToHash("PlayerRunLeft");
        private static readonly int playerAttack   = Animator.StringToHash("PlayerAttack");
        private static readonly int playerIdle     = Animator.StringToHash("PlayerIdle");

        private int _countViewInfo;

        private void Start() {
            _trigger         = FindObjectOfType<PlayerTrigger>();
            _controller      = FindObjectOfType<PlayerController>();
            _playerAnimation = FindObjectOfType<PlayerAnimation>();
            _dialogWindow    = FindObjectOfType<DialogWindow>();
            _sound           = FindObjectOfType<Sounds>();
        }

        private void Update() {
            CloseWindowsIfHorizontal();
            TakeItemIfE();
            InfoMessage();

            if(!_trigger.IsItemNear && Input.GetKeyDown(KeyCode.E)) {
                if(_controller.LeftHand.transform.childCount <= 0) { return; }

                GameObject item = _controller.LeftHand.transform.GetChild(0).gameObject;
                _controller.ThrowTheObjectOutOfHand(item);
            }

            if(Input.GetKeyDown(KeyCode.A)) {
                if(_playerAnimation.FacingRight) {
                    _playerAnimation.Flip();
                    _playerAnimation.FacingRight = false;
                }

                // _dialogWindow.CloseWindow();
                _playerAnimation.Animator.SetTrigger(playerRunLeft);
            }

            if(Input.GetKeyUp(KeyCode.A)) { _playerAnimation.Animator.SetTrigger(playerIdle); }

            if(Input.GetKeyDown(KeyCode.A)) {
                if(_playerAnimation.FacingRight) {
                    _playerAnimation.Flip();
                    _playerAnimation.FacingRight = false;
                }

                // _dialogWindow.CloseWindow();
                _playerAnimation.Animator.SetTrigger(playerRunLeft);
            }

            if(Input.GetKeyUp(KeyCode.A)) { _playerAnimation.Animator.SetTrigger(playerIdle); }

            if(Input.GetKeyDown(KeyCode.D)) {
                if(!_playerAnimation.FacingRight) {
                    _playerAnimation.Flip();
                    _playerAnimation.FacingRight = true;
                }

                // _dialogWindow.CloseWindow();
                _playerAnimation.Animator.SetTrigger(playerRunRight);
            }

            if(Input.GetKeyUp(KeyCode.D)) { _playerAnimation.Animator.SetTrigger(playerIdle); }

            if(Input.GetKeyDown(KeyCode.C)) {
                // _dialogWindow.CloseWindow();
                _playerAnimation.Animator.SetTrigger(playerAttack);
            }

            if(Input.GetKeyUp(KeyCode.C)) { _playerAnimation.Animator.SetTrigger(playerIdle); }

            if(Input.GetKeyDown(KeyCode.Space)) {
                // _dialogWindow.CloseWindow();
                _sound.PlaySoundJump();
            }
        }

        private void CloseWindowsIfHorizontal() {
            if(Input.GetButton("Horizontal")) {
                if(_dialogWindow.IsWindowOpen) {
                    _dialogWindow.CloseWindow();    
                }
                _sound.PlayStepSound();
            }
        }

        private void TakeItemIfE() {
            if(_trigger.IsItemNear && Input.GetKeyDown(KeyCode.E)) {
                Item item = _trigger.CurrentItem.gameObject.GetComponent<Item>();
                _trigger.ShowText(item.getDescription());
                _trigger.TakeItem();
                _sound.PlaySoundTakeItem();
            }
        }

        private void InfoMessage() {
            if(_trigger.CurrentInfo) {
                Info info = _trigger.CurrentInfo.gameObject.GetComponent<Info>();
                _trigger.ShowText(info.GetDescription());
            }
        }
    }
}
