﻿using UnityEngine;

namespace Player {
    public class PlayerKeyboardController : MonoBehaviour {
 
        public PlayerAlt PlayerAlt;
   
        private void Start () {
 
            PlayerAlt = PlayerAlt == null ? GetComponent<PlayerAlt>() : PlayerAlt;
            if(PlayerAlt == null)
            {
                Debug.LogError("PlayerAlt не установлен в контроллер.");
            }
        }
 
        private void Update () {
 
            if (PlayerAlt != null)
            {
 
                if (Input.GetKey(KeyCode.D))
                {
                    PlayerAlt.MoveRight();
                }
                if (Input.GetKey(KeyCode.A))
                {
                    PlayerAlt.MoveLeft();
                }
                if (Input.GetKeyDown(KeyCode.Space))
                {
                    PlayerAlt.Jump();
                }
            }
        }
    }
}
