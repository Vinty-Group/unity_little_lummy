﻿using UnityEngine;

namespace Player{
    public class PlayerAnimation : MonoBehaviour{
        public bool     FacingRight {get; set;} = true;
        public Animator Animator    {get; set;}

        private void Start() {
            Animator = GetComponent<Animator>();
        }

        public void Flip() {
            Animator.transform.Rotate(0, 180, 0);
        }
    }
}
