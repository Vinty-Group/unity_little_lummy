﻿using System;
using InfoPanels;
using Items;
using UnityEditor;
using UnityEngine;

namespace Player {
    public class PlayerTrigger : MonoBehaviour {
        private ItemBar          _itemBar;
        private DialogWindow     _dialogWindow;
        private HitBullet        _hitBullet;
        private GameManager      _gameManager;
        private MovingController _movingController;
        private Bomb             _bomb;
        private int              _countView;
        private int              _viewCount;
        public  bool             IsItemNear  {get; private set;}
        public  bool             IsInfoNear  {get; private set;}
        public  GameObject       CurrentItem {get; private set;}
        public  GameObject       CurrentInfo {get; private set;}

        private void Start() {
            //        PlayerPrefs.DeleteKey("HammerCount");
            _dialogWindow     = FindObjectOfType<DialogWindow>();
            _itemBar          = FindObjectOfType<ItemBar>();
            _hitBullet        = FindObjectOfType<HitBullet>();
            _gameManager      = FindObjectOfType<GameManager>();
            _movingController = FindObjectOfType<MovingController>();
            _bomb             = FindObjectOfType<Bomb>();
            _countView        = 0;
        }

        private void OnTriggerEnter2D(Collider2D other) {
            string myTag   = other.tag;
            string objName = other.gameObject.name;

            CheckTag(other, myTag);
            CheckObjName(objName);
            CheckPoint(objName);
        }

        private void OnTriggerStay2D(Collider2D other) {
            string objName = other.gameObject.name;

            CheckObjNameForStayTriggers(objName);
        }

        private void OnTriggerExit2D(Collider2D other) {
            IsItemNear  = false;
            CurrentItem = null;

            IsInfoNear  = false;
            CurrentInfo = null;

            _movingController.Speed = 5.5f;
        }
        // При нахождении Героя внутри триггера 
        private void CheckObjNameForStayTriggers(string objName) {
            switch(objName) {
                case"Web-collider" :
                    _movingController.Speed = 3f;
                    break;
            }
        }

        private void CheckObjName(string objName) {
            if(_hitBullet == null) {
                return;
            }

            switch(objName) {
                case"InvisibleWallTowerOn" :
                    _hitBullet.isTowerLive  = true;
                    _hitBullet.IsPlayerNear = true;
                    break;
                case"InvisibleWallTowerOff" :
                    _hitBullet.StopTowerAttack();
                    break;
                case"Bomb" :
                    _bomb.BombActivated();
                    break;
            }
        }

        private void CheckTag(Collider2D other, string myTag) {
            switch(myTag) {
                case"Item" :
                    IsItemNear  = true;
                    CurrentItem = other.gameObject;
                    break;
                case"Info" :
                    IsItemNear  = true;
                    CurrentInfo = other.gameObject;
                    break;
                case"Death" :
                    _gameManager.MinusLife();
                    break;
            }
        }

        private void CheckPoint(string objName) {
            if(objName.Length >= 6) {
                string respSubstringName  = objName.Substring(0, 4);
                string respSubstringIndex = objName.Substring(5, 1);
                switch(respSubstringName) {
                    case"Resp" :
                        _gameManager.IndexRespawn = Convert.ToInt32(respSubstringIndex);
                        break;
                }
            }
        }

        public void ShowText(string textMessage) {
            _dialogWindow.SetText(textMessage);
            _dialogWindow.OpenWindow();
        }

        public void TakeItem() {
            _itemBar.setItemToFreeCell(CurrentItem);
        }
    }
}
