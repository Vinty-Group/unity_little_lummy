﻿using Items;
using UnityEngine;

namespace Player {
    public class PlayerController : MonoBehaviour {
        private GameObject _leftHand;
        private Transform  _itemsStorage;
        private ItemBar    _itemBar;
        public  int        Health     {get;}
        public  GameObject ItemInHand {get; set;}

        public GameObject LeftHand {
            get => _leftHand;
        }

        private GameObject _item;
        private Vector3    _positionItemInTheLeftHand;
        private bool       _isLeftHandIsEmpty = true;

        private readonly Vector3    _pos                          = new Vector3(3.76f, -3.05f, 0);
        private readonly Quaternion _rotateItemInHandForLookRight = new Quaternion(0f, 0f, -20f, 65);

        public PlayerController(int health, GameObject itemInHand) {
            Health     = health;
            ItemInHand = itemInHand;
        }

        private void Start() {
            _itemBar      = FindObjectOfType<ItemBar>();
            _itemsStorage = GameObject.Find("ItemsStorage").transform;
            _leftHand     = GameObject.Find("leftHand");
        }

        public GameObject TakeItInTheLeftHand(GameObject item) {
            _item = item;
            ReplaceInTheLeftHand(_item);
            return _item;
        }

        private void Update() {
            if(_leftHand.transform.childCount == 0) { _isLeftHandIsEmpty = true; }
        }

        public void ReplaceInTheLeftHand(GameObject itemForLeftHand) {
            int countItemsInHand = _leftHand.transform.childCount;
            if(countItemsInHand == 0) {
                transformItemToLeftHand(itemForLeftHand);
                SetLeftHandBusy();
                ItemInHand = itemForLeftHand;
            }
            else {
                // его надо сначала выложить в свободную ячейку инвенторя
                GameObject currentItemInLeftHand = _leftHand.transform.GetChild(0).gameObject;
                _itemBar.setItemToFreeCell(currentItemInLeftHand.gameObject);
                transformItemToLeftHand(itemForLeftHand);
                SetLeftHandBusy();
            }
        }

        public void ThrowTheObjectOutOfHand(GameObject itemInHand) {
            itemInHand.transform.SetParent(_itemsStorage);
            Vector3 position = _leftHand.transform.position;
            itemInHand.transform.position    = new Vector3(position.x + 0.5f, position.y - 0.8f, 0f);
            itemInHand.transform.eulerAngles = _positionItemInTheLeftHand;
            ;
            IsLeftHandIsEmpty();
        }

        private void transformItemToLeftHand(GameObject itemForLeftHand) {
            itemForLeftHand.transform.SetParent(_leftHand.transform);
            itemForLeftHand.transform.position      = _leftHand.transform.position;
            itemForLeftHand.transform.localPosition = _pos;
            itemForLeftHand.transform.localRotation = _rotateItemInHandForLookRight;
        }

        private bool IsLeftHandIsEmpty() {
            return _isLeftHandIsEmpty;
        }

        public void SetLeftHandBusy() {
            _isLeftHandIsEmpty = false;
        }
    }
}
