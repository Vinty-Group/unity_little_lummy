﻿using System;
using UnityEngine;

namespace Player {
    public class MovingController : MonoBehaviour {
        [SerializeField] private float      jumpImpuls;
        [SerializeField] private Transform  groundChek;
        [SerializeField] private float      chekRadius;
        [SerializeField] private LayerMask  whatIsGrounded;
        [SerializeField] private int        extraJumpValue;
        [SerializeField] private GameObject _objMountains;
        [SerializeField] private GameObject _objMountains2;
        [SerializeField] private GameObject _objCloud;

        private Rigidbody2D _rb;
        private Rigidbody2D _rbMountains;
        private Rigidbody2D _rbMountains2;
        private Rigidbody2D _rbCloud;
        private float       _moveInput;
        private bool        _facingRight;
        private bool        _isGrounded;
        private int         _extraJump;

        public float Speed {get; set;}

        private void Start() {
            _facingRight  = true;
            _extraJump    = extraJumpValue;
            _rb           = GetComponent<Rigidbody2D>();
            _rbMountains  = _objMountains.GetComponent<Rigidbody2D>();
            _rbMountains2 = _objMountains2.GetComponent<Rigidbody2D>();
            _rbCloud      = _objCloud.GetComponent<Rigidbody2D>();
            Speed         = 5.5f;
        }

        private void FixedUpdate() {
            MoveAxis();
        }
        // Плохо работает в FixedUpdate
        private void Update() {
            Jump();
        }

        private void Jump() {
            if(_isGrounded) {
                _extraJump = extraJumpValue;
            }

            if(Input.GetKeyDown(KeyCode.Space) && _extraJump > 0) {
                _rb.velocity = Vector2.up * jumpImpuls;
                _extraJump--;
            }
            else if(Input.GetKeyDown(KeyCode.Space) && _extraJump == 0 && _isGrounded) {
                _rb.velocity = Vector2.up * jumpImpuls;
            }
        }

        private void MoveAxis() {
            _isGrounded  = Physics2D.OverlapCircle(groundChek.position, chekRadius, whatIsGrounded);
            _moveInput   = Input.GetAxis("Horizontal");
            _rb.velocity = new Vector2(_moveInput * Speed, _rb.velocity.y);
            _rbMountains.velocity = new Vector2(
                _moveInput * Mathf.Lerp(Speed / 2, Speed, Time.deltaTime * 2f) * 0.07f,
                _rbMountains.velocity.y
            );
            _rbMountains2.velocity = new Vector2(
                _moveInput * Mathf.Lerp(Speed / 2, Speed, Time.deltaTime * 2f) * 0.05f,
                _rbMountains2.velocity.y
            );
            _rbCloud.velocity = new Vector2(_moveInput * Mathf.Lerp(Speed / 2, Speed, Time.deltaTime * 2f) * 0.09f,
                                            _rbCloud.velocity.y
            );
        }
    }
}
