﻿using LevelRedactor;
using UnityEngine;

public class CreateObj : MonoBehaviour {
    [SerializeField] private PlatformData _platformData;
    private                  bool         _isGroup;
    private                  GameObject   parent;

    private void Start() {
        // РАБОТАЕТ. Создает платформу из Даты   
        // parent = new GameObject("Parent");
        // CreatePlatform();
    }

    [ContextMenu("Create Platform")]
    public void CreatePlatform() {
        _isGroup = _platformData.Group_by;
        // newParent.transform.position = Vector2.zero;
        Vector2    position     = Vector2.zero;
        GameObject leftPlatform = Instantiate(_platformData._leftPlatform);
        leftPlatform.transform.position =  position;
        position                        += new Vector2(1f, 0f);
        if(_isGroup) {
            leftPlatform.transform.SetParent(parent.transform);
        }

        for(int i = 0 ;
            i < _platformData._countCenterElements ;
            i++) {
            GameObject centerPlatform = Instantiate(_platformData._centerPlatform);
            centerPlatform.transform.position =  position;
            position                          += new Vector2(1f, 0f);
            if(_isGroup) {
                centerPlatform.transform.SetParent(parent.transform);
            }
        }

        GameObject rightPlatform = Instantiate(_platformData._rightPlatform);
        rightPlatform.transform.position =  position;
        position                         += new Vector2(1f, 0f);
        if(_isGroup) {
            rightPlatform.transform.SetParent(parent.transform);
        }
    }
}
