﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



using UnityEngine;
using UnityEditor;
public class PlatformMenu : EditorWindow
{

    [MenuItem("Window/Меню платформ")]
    static void Init()
    {    
        GetWindow<PlatformMenu>();
    }

    void OnGUI()
    {
        if(GUILayout.Button("Create Platform"))
        {
            GameObject generatorPlatforms = GameObject.Find("GeneratorPlatform");
            CreateObj createObj = generatorPlatforms.GetComponent<CreateObj>();
            createObj.CreatePlatform();
        }
    }
}
