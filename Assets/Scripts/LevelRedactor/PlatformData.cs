﻿using UnityEngine;
using UnityEngine.Serialization;

namespace LevelRedactor {
    [CreateAssetMenu(fileName =  "PlatformData", menuName =  "Platforms/DifferentPlatforms", order = 1)]
    public class PlatformData : ScriptableObject
    {
        [SerializeField] public GameObject _leftPlatform;
        [SerializeField] public GameObject _centerPlatform;
        [SerializeField] public GameObject _rightPlatform;
        public string Name = "New Platform Name";
        public int _countCenterElements;
        [FormerlySerializedAs("GroupBy")] public bool Group_by;
    }
}
