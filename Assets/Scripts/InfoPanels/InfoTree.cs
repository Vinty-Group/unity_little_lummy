﻿namespace InfoPanels{
    public class InfoTree : Info{
        private const string NAME        = "Растущее Дерево";
        private const string DESCRIPTION = "Из этого дерева получилось бы много бревен и досок. ";

        public override string GetName() {
            return NAME;
        }

        public override string GetDescription() {
            return DESCRIPTION;
        }

        public InfoTree(string name, string description) : base(name, description) {
        }
    }
}
