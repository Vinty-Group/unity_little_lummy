﻿using TMPro;
using UnityEngine;

namespace InfoPanels {
    public class DialogWindow : MonoBehaviour {
        private GameObject _textDialogWindow;
        private GameObject _closeBtn;
        private bool       _isWindowOpen;

        public bool IsWindowOpen {
            get => _isWindowOpen;
            set => _isWindowOpen = value;
        }

        private int _childCount;

        private void Start() {
            _closeBtn         = GameObject.Find("BtnCloseDialog");
            _textDialogWindow = GameObject.Find("PlayerTextDialogWindow");
            _textDialogWindow.GetComponent<TextMeshPro>().SetText("");
            _childCount = transform.childCount;
        }

        public void OpenWindow() {
            transform.GetComponent<Renderer>().enabled = true;
            SwitсhWindow(true);
            _isWindowOpen = true;
        }

        public void CloseWindow() {
            SetText("");
            SwitсhWindow(false);
            _isWindowOpen = false;
        }

        public void SetText(string text) {
            _textDialogWindow.GetComponent<TextMeshPro>().SetText(text);
        }

        private void SwitсhWindow(bool switchOnOrSwitchOff) {
            transform.GetComponent<Renderer>().enabled = switchOnOrSwitchOff;
            for(int i = 0 ;
                i < _childCount ;
                i++) {
                GameObject obj = transform.GetChild(i).gameObject;
                obj.GetComponent<Renderer>().enabled = switchOnOrSwitchOff;
            }
        }
    }
}
