﻿using UnityEngine;

namespace InfoPanels{
    public abstract class Info : MonoBehaviour{
        private readonly string _name;
        private readonly string _description;

        protected Info(string name, string description) {
            _name        = name;
            _description = description;
        }
        
        public new virtual string GetName() {
            return _name;
        }

        public new virtual string GetDescription() {
            return _description;
        }
    }
}
