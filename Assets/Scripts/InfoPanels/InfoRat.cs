﻿namespace InfoPanels {
    public class InfoRat : Info {
        private const string NAME = "Злая крыса";

        private const string DESCRIPTION = "Крыса ни за что не даст мне пройти. " +
                                           "Ее надо как-то изловить. Нужна ловушка для крысы";

        public override string GetName() {
            return NAME;
        }

        public override string GetDescription() {
            return DESCRIPTION;
        }

        public InfoRat(string name, string description) : base(name, description) {
        }
    }
}
